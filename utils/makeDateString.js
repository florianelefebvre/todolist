export const makeDateString = (dateObject) => {
  const dateString = dateObject.toLocaleDateString("fr-FR", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });
  return dateString;
};
