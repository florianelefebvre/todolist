import { types as typesTasks } from "../actions/tasks.actions";

const initialState = {
  allTasks: [],
  displayedTask: null,
  taskIndex: null,
  newTaskTitle: "",
  showDatePicker: false,
  showPeoplePicker: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case typesTasks.CREATE:
      var newAllTasks = [...state.allTasks, action.payload];
      return { ...state, allTasks: newAllTasks };

    case typesTasks.UPDATE:
      let taskToUpdate = state.allTasks[action.payload.index];
      let keysToModify = Object.keys(action.payload);
      for (let i = 0; i < keysToModify.length; i++) {
        if (keysToModify[i] !== "index") {
          Object.defineProperty(taskToUpdate, keysToModify[i], {
            value: action.payload[keysToModify[i]],
          });
        }
      }
      var newAllTasks = [...state.allTasks];
      newAllTasks.splice(action.payload.index, 1, taskToUpdate);
      return { ...state, allTasks: newAllTasks };

    case typesTasks.DISPLAY:
      return {
        ...state,
        displayedTask: state.allTasks[action.payload.index],
      };

    case typesTasks.DISPLAY_NONE:
      return { ...state, displayedTask: null };

    case typesTasks.NEW_TASK_TITLE:
      return { ...state, newTaskTitle: action.payload };

    case typesTasks.SET_INDEX:
      return { ...state, taskIndex: action.payload };

    case typesTasks.SHOW_DATE_PICKER:
      return { ...state, showDatePicker: action.payload };

    case typesTasks.SHOW_PEOPLE_PICKER:
      return { ...state, showPeoplePicker: action.payload };

    default:
      return state;
  }
};
export default reducer;
