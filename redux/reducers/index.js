import { combineReducers } from "redux";
import tasks from "./tasks.reducers";

export default combineReducers({
  tasks,
});
