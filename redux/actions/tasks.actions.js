export const types = {
  CREATE: "tasks/CREATE",
  UPDATE: "tasks/UPDATE",
  DISPLAY: "tasks/DISPLAY",
  DISPLAY_NONE: "tasks/DISPLAY_NONE",
  NEW_TASK_TITLE: "tasks/NEW_TASK_TITLE",
  SET_INDEX: "tasks/SET_INDEX",
  SHOW_DATE_PICKER: "tasks/FOCUS_CHANGE",
  SHOW_PEOPLE_PICKER: "tasks/SHOW_PEOPLE_PICKER",
};

export function createTask(payload) {
  return {
    type: types.CREATE,
    payload,
  };
}

export function updateTask(payload) {
  return {
    type: types.UPDATE,
    payload,
  };
}

export function displayTask(payload) {
  return {
    type: types.DISPLAY,
    payload,
  };
}

export function displayNoTask(payload) {
  return {
    type: types.DISPLAY_NONE,
    payload,
  };
}

export function setNewTaskTitle(payload) {
  return {
    type: types.NEW_TASK_TITLE,
    payload,
  };
}

export function setIndex(payload) {
  return {
    type: types.SET_INDEX,
    payload,
  };
}

export function setShowDatePicker(payload) {
  return {
    type: types.SHOW_DATE_PICKER,
    payload,
  };
}

export function setShowPeoplePicker(payload) {
  return {
    type: types.SHOW_PEOPLE_PICKER,
    payload,
  };
}
