// actions
import { useSelector, useDispatch } from "react-redux";
import {
  updateTask,
  displayNoTask,
  setShowDatePicker,
  setShowPeoplePicker,
} from "../redux/actions/tasks.actions";

// components
import RoundButton from "./RoundButton";
import PeoplePicker from "./PeoplePicker";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";

// styles
import {
  StyledSelectedTask,
  Title,
  ButtonsSection,
  FinishedLabel,
  SectionTitle,
  StyledCalendar,
} from "../styles/selectedTask.styles";

import { Icon } from "../styles/icons.styles";

import {
  DescriptionSection,
  DescriptionTitle,
  DescriptionInput,
  SubmitButton,
  WrapSubmitButton,
} from "../styles/descriptionSection.styles";

const SelectedTask = () => {
  const dispatch = useDispatch();

  const displayedTaskTitle = useSelector(
    (state) => state.tasks.displayedTask.title
  );
  const displayedTaskDescription = useSelector(
    (state) => state.tasks.displayedTask.description
  );
  const taskIndex = useSelector((state) => state.tasks.taskIndex);
  const taskStatus = useSelector((state) => state.tasks.displayedTask.status);
  const showDatePicker = useSelector((state) => state.tasks.showDatePicker);
  const showPeoplePicker = useSelector((state) => state.tasks.showPeoplePicker);
  const deadline = useSelector((state) => state.tasks.displayedTask.deadline);

  return (
    <StyledSelectedTask>
      <SectionTitle>
        <Title
          placeholder="Ajouter un titre"
          value={displayedTaskTitle}
          onChange={(event) => {
            dispatch(
              updateTask({ index: taskIndex, title: event.target.value })
            );
          }}
        ></Title>
        {taskStatus && (
          <FinishedLabel>
            <Icon grey className="fas fa-check" fontSize20></Icon>
            <span>MARQUÉ COMME TERMINÉ</span>
          </FinishedLabel>
        )}
      </SectionTitle>

      <ButtonsSection>
        <RoundButton
          disabled={taskStatus}
          buttonType="ATTRIBUER A"
          icon={<Icon fontSize15 grey className="fas fa-plus"></Icon>}
          onClickFunction={() => {
            dispatch(setShowPeoplePicker(!showPeoplePicker));
            dispatch(setShowDatePicker(false));
          }}
        ></RoundButton>

        {/* PEOPLE PICKER */}

        {showPeoplePicker && <PeoplePicker></PeoplePicker>}
        <RoundButton
          disabled={taskStatus}
          buttonType="ECHEANCE"
          deadline={deadline}
          icon={<Icon fontSize15 grey className="far fa-clock"></Icon>}
          onClickFunction={() => {
            dispatch(setShowDatePicker(!showDatePicker));
            dispatch(setShowPeoplePicker(false));
          }}
        ></RoundButton>

        {/*  DATE PICKER  */}

        {showDatePicker && (
          <StyledCalendar>
            <Calendar
              onClickDay={(day) => {
                const newDeadline = new Date(day).toLocaleDateString("fr-FR", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                });
                // const today = new Date(Date.now()).toLocaleDateString("fr-FR", {
                //   year: "numeric",
                //   month: "short",
                //   day: "numeric",
                // });

                dispatch(
                  updateTask({ index: taskIndex, deadline: newDeadline })
                );
                dispatch(setShowDatePicker(false));
              }}
            ></Calendar>
          </StyledCalendar>
        )}
      </ButtonsSection>
      <DescriptionSection>
        <DescriptionTitle>
          <i className="far fa-edit"></i>
          <p>Description</p>
        </DescriptionTitle>
        <DescriptionInput
          disabled={taskStatus}
          value={displayedTaskDescription}
          onChange={(event) => {
            dispatch(
              updateTask({ index: taskIndex, description: event.target.value })
            );
          }}
        ></DescriptionInput>
      </DescriptionSection>
      {!taskStatus && (
        <WrapSubmitButton>
          <SubmitButton
            onClick={() => {
              dispatch(displayNoTask());
            }}
          >
            ENREGISTRER
          </SubmitButton>
        </WrapSubmitButton>
      )}
    </StyledSelectedTask>
  );
};

export default SelectedTask;
