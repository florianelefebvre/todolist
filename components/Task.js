// styles
import {
  Checkbox,
  StyledTask,
  CreatedAt,
  TitleDescription,
} from "../styles/task.styles";

import { Icon } from "../styles/icons.styles";
import { makeDateString } from "../utils/makeDateString";
// actions
import {
  updateTask,
  displayTask,
  setIndex,
} from "../redux/actions/tasks.actions";

import { useSelector, useDispatch } from "react-redux";

const Task = ({ task, index }) => {
  const dispatch = useDispatch();

  const taskStatus = useSelector((state) => state.tasks.allTasks[index].status);
  const today = makeDateString(new Date(Date.now()));

  return (
    <div
      onClick={() => {
        dispatch(setIndex(index));
        dispatch(displayTask({ index }));
      }}
    >
      {taskStatus ? (
        <StyledTask>
          <Checkbox
            orange
            onClick={() => {
              dispatch(
                updateTask({
                  index,
                  status: !task.status,
                })
              );
            }}
          >
            <Icon className="fas fa-check" fontSize15></Icon>
          </Checkbox>
          <TitleDescription lineTrough>
            <h2>{task.title}</h2>
            <p>{task.userName}</p>
          </TitleDescription>
          <CreatedAt>
            {today === task.createdAt ? "Aujourd'hui" : task.createdAt}
          </CreatedAt>
        </StyledTask>
      ) : (
        <StyledTask>
          <Checkbox
            onClick={() => {
              dispatch(
                updateTask({
                  index,
                  status: !task.status,
                })
              );
            }}
          ></Checkbox>
          <TitleDescription>
            <h2>{task.title}</h2>

            {task.attributedTo !== "ATTRIBUER À" && <p>{task.attributedTo}</p>}
          </TitleDescription>
          <CreatedAt>
            {today === task.createdAt ? "Aujourd'hui" : task.createdAt}
          </CreatedAt>
        </StyledTask>
      )}
    </div>
  );
};

export default Task;
