import { StyledPeoplePicker, Person } from "../styles/peoplePicker.styles";
import peopleList from "../public/fakeUsersList/data/randomuser.json";
import { useSelector, useDispatch } from "react-redux";

// actions
import {
  updateTask,
  setShowPeoplePicker,
} from "../redux/actions/tasks.actions";

const PeoplePicker = () => {
  const dispatch = useDispatch();

  const taskIndex = useSelector((state) => state.tasks.taskIndex);

  return (
    <StyledPeoplePicker>
      {peopleList.map((person, index) => {
        return (
          <Person
            key={index}
            onClick={() => {
              dispatch(
                updateTask({
                  index: taskIndex,
                  attributedTo: `${
                    person.first_name.charAt(0).toUpperCase() +
                    person.first_name.slice(1)
                  } ${
                    person.last_name.charAt(0).toUpperCase() +
                    person.last_name.slice(1)
                  }
                   `,
                })
              );
              dispatch(setShowPeoplePicker(false));
            }}
          >
            <img src={`assets/pictures/${person.picture}`} />
            <p>
              {person.first_name.charAt(0).toUpperCase() +
                person.first_name.slice(1)}
            </p>
            <p>
              {person.last_name.charAt(0).toUpperCase() +
                person.last_name.slice(1)}
            </p>
          </Person>
        );
      })}
    </StyledPeoplePicker>
  );
};

export default PeoplePicker;
