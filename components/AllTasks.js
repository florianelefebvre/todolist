// styles
import {
  StyledAllTasks,
  Title,
  AddTask,
  FinishedTasks,
} from "../styles/allTasks.styles";

// components
import Task from "./Task";

// actions
import { createTask, setNewTaskTitle } from "../redux/actions/tasks.actions";

// functions
import { useSelector, useDispatch } from "react-redux";
import { makeDateString } from "../utils/makeDateString";

const AllTasks = () => {
  const dispatch = useDispatch();

  const tasks = useSelector((state) => state.tasks.allTasks);
  const newTaskTitle = useSelector((state) => state.tasks.newTaskTitle);
  const countTasksDone = tasks.filter((task) => task.status === true).length;

  return (
    <StyledAllTasks>
      <Title>Toutes les taches</Title>
      <AddTask
        placeholder="+ Ajouter une tache"
        value={newTaskTitle}
        onChange={(event) => {
          dispatch(setNewTaskTitle(event.target.value));
        }}
        onKeyPress={(event) => {
          if (event.key === "Enter") {
            if (newTaskTitle) {
              dispatch(
                createTask({
                  title: newTaskTitle,
                  description: "",
                  deadline: "",
                  createdAt: makeDateString(new Date(Date.now())),
                  attributedTo: "ATTRIBUER À",
                  status: false,
                  deadline: "ECHEANCE",
                })
              );
              dispatch(setNewTaskTitle(""));
            } else {
              alert("Merci de saisir un titre");
            }
          }
        }}
      />

      {tasks.map((task, index) => {
        if (task.status === false) {
          return <Task key={index} task={task} index={index}></Task>;
        }
      })}

      <FinishedTasks>
        <div>Taches terminées</div>
        <div>{countTasksDone}</div>
      </FinishedTasks>
      {tasks.map((task, index) => {
        if (task.status === true) {
          return <Task key={index} task={task} index={index}></Task>;
        }
      })}
    </StyledAllTasks>
  );
};

export default AllTasks;
