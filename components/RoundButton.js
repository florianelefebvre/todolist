import { StyledRoundButton, Circle } from "../styles/roundButton.styles";
import { useSelector } from "react-redux";

const RoundButton = ({ icon, onClickFunction, buttonType, disabled }) => {
  const deadline = useSelector((state) => state.tasks.displayedTask.deadline);
  const attributedTo = useSelector(
    (state) => state.tasks.displayedTask.attributedTo
  );

  return (
    <StyledRoundButton onClick={onClickFunction} disabled={disabled}>
      <Circle>{icon}</Circle>
      {buttonType === "ECHEANCE" ? <p>{deadline} </p> : <p>{attributedTo} </p>}
    </StyledRoundButton>
  );
};

export default RoundButton;
