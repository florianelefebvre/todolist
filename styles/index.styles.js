import styled from "styled-components";

export const StyledHome = styled.div`
  display: flex;
  flex-direction: row;
  min-height: calc(100vh - 64px);
  flex: 1;
`;
