import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`

   * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight:400;
    
  }
  /* input {
    &:focus {
    outline: none;
  } */
  /* } */

`;
