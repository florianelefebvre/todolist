import styled from "styled-components";

export const Header = styled.div`
  width: 100vw;
  height: 64px;
  /* background-color: red; */
  border: solid #e4e8eb 2px;
`;

export const Logo = styled.img`
  width: 120px;
  height: 47px;
  margin-top: 7px;
  margin-left: 5px;
`;
