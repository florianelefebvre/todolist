import styled from "styled-components";

export const StyledRoundButton = styled.button`
  width: 148px;
  height: 42px;
  color: #aeafaf;
  border-radius: 21px;
  border: none;
  font-size: 12px;
  line-height: 16px;
  margin-left: 34px;
  display: flex;
  align-items: center;
  font-family: "Open Sans";
  background-color: #ffffff;
  :hover {
    cursor: pointer;
  }
`;

export const Circle = styled.div`
  width: 28px;
  height: 28px;
  border-radius: 50%;
  border: dashed 2px #979797;
  font-weight: 500;
  font-size: 16px;
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
