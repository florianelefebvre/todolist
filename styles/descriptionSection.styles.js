import styled from "styled-components";

export const DescriptionSection = styled.div`
  width: calc(100% - 36px * 2);
  margin-top: 36px;
  margin-left: 36px;
  height: 240px;
  border: solid 2px #e4e8eb;
  background-color: white;
`;

export const DescriptionTitle = styled.div`
  font-weight: 500px;
  font-size: 14px;
  line-height: 20px;
  color: #252627;
  margin: 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
  p {
    margin-left: 10px;
  }
`;

export const DescriptionInput = styled.textarea`
  width: calc(100% - 41px * 2);
  margin: 0 41px;
  height: 164px;
  border: solid 2px #e4e8eb;
  font-weight: 400;
  font-size: 14px;
  color: #212223;
  padding: 14px;

  &:focus {
    outline: none;
  }
`;

export const WrapSubmitButton = styled.div`
  width: 100%;
  background-color: white;
  width: calc(100% - 36px * 2);
  margin-left: 36px;
  border: solid 2px #e4e8eb;
  border-top: none;
  display: flex;
  justify-content: end;
`;

export const SubmitButton = styled.button`
  width: 120px;
  height: 32px;
  padding: 10px;
  margin: 10px;
  border: none;
  background-color: #e74c3c;
  font-weight: 500;
  font-size: 12px;
  color: white;
  margin-right: 36px;
  :hover {
    cursor: pointer;
  }
`;
