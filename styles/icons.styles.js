import styled from "styled-components";

// export const CheckIcon = styled.i`
//   font-size: ${(props) => props.fontSize15 && "15px"};
//   font-size: ${(props) => props.fontSize20 && "20px"};
//   color: ${(props) => (props.grey ? "#9B9C9C" : "white")};
// `;

export const Icon = styled.i`
  font-size: ${(props) => props.fontSize15 && "15px"};
  font-size: ${(props) => props.fontSize20 && "20px"};
  color: ${(props) => (props.grey ? "#9B9C9C" : "white")};
`;

{
  /* <i class="fas fa-plus"></i> */
}
