import styled from "styled-components";

export const StyledPeoplePicker = styled.div`
  background-color: purple;
  position: absolute;
  top: 200px;
  left: 400px;
  height: 400px;
  overflow-y: scroll;
`;

export const Person = styled.div`
  background-color: white;
  width: 250px;
  display: flex;
  flex-direction: row;
  padding: 10px;
  align-items: center;
  :hover {
    cursor: pointer;
  }
  img {
    width: 50px;
    border-radius: 50%;
  }
  p {
    margin-left: 10px;
  }
  /* border-bottom: 1px black solid; */
`;
