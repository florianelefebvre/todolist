import styled from "styled-components";

export const StyledSelectedTask = styled.div`
  background-color: #f7f8f9;
  flex: 1;
`;
export const SectionTitle = styled.div`
  height: 60px;
  min-width: 100%;
  background-color: #ffffff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 16px 26px;
`;
export const Title = styled.input`
  font-size: 22px;
  line-height: 28px;
  font-weight: 400;
  color: #252627;
  border: none;
  &:focus {
    outline: none;
  }
`;

export const FinishedLabel = styled.div`
  width: 202px;
  height: 32px;
  color: #aeafaf;
  background-color: #ffffff;
  border-radius: 3px;
  border: solid #e4e8eb 1px;
  font-size: 12px;
  line-height: 14px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  font-family: "Open Sans";
`;

export const ButtonsSection = styled.div`
  height: 60px;
  /* background-color: blue; */

  display: flex;
  align-items: end;
`;

export const StyledCalendar = styled.div`
  position: absolute;
  top: 200px;
  left: 580px;
`;
