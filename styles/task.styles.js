import styled from "styled-components";

export const StyledTask = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  padding: 20px 15px;
  flex-direction: row;
  justify-content: space-between;
  font-size: 14px;
  :hover {
    cursor: pointer;
  }
`;

export const TitleDescription = styled.div`
  width: 200px;
  padding: 0 11px;
  font-size: 14px;
  line-height: 20px;
  /* background-color: red; */
  h2 {
    font-size: 14px;
    font-weight: 700;
    color: #292b2c;
    text-decoration: ${(props) => (props.lineThrough ? "line-through" : "")};
  }
  p {
    font-weight: 400;
    font-size: 13px;
    color: #252627;
    text-decoration: ${(props) => (props.lineThrough ? "line-through" : "")};
  }
`;

export const CreatedAt = styled.p`
  font-weight: 400;
  font-size: 12px;
  color: #a5a6a6;
  text-decoration: ${(props) => (props.lineThrough ? "line-through" : "")};
`;

export const Checkbox = styled.span`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  :hover {
    cursor: pointer;
  }
  border: ${(props) => (props.orange ? "none" : "#979797 1px solid")};
  background: ${(props) => (props.orange ? "#E74C3C" : "white")};
  display: flex;
  justify-content: center;
  align-items: center;
`;
