import styled from "styled-components";

export const StyledAllTasks = styled.div`
  width: 360px;
  /* background-color: green; */
`;

export const Title = styled.h1`
  height: 60px;
  font-size: 16px;
  line-height: 28px;
  font-weight: 900;
  color: white;
  background-color: #e74c3c;
  padding: 16px 21px;
`;

export const AddTask = styled.input`
  height: 60px;
  font-size: 16px;
  line-height: 28px;
  font-weight: 500;
  background-color: #ffffff;
  border: none;
  border-bottom: solid #e4e8eb 2px;
  color: #c4c4c4;
  padding: 16px 21px;
  min-width: 100%;
  text-align: left;
  &:focus {
    outline: none;
  }
`;

export const FinishedTasks = styled.div`
  width: 100%;
  height: 36px;
  color: #dc4d3c;
  background-color: #fbeceb;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 26px;
  margin-top: 7px;
`;
