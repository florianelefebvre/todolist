import { GlobalStyle } from "../styles/Global.styles";
import Head from "next/head";
import { wrapper } from "../redux/store";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto:wght@400;500;700;900&display=swap"
          rel="stylesheet"
        />
        <script src="https://kit.fontawesome.com/bb54014cd5.js"></script>
      </Head>
      <GlobalStyle />
      <Component {...pageProps} />
    </div>
  );
}

export default wrapper.withRedux(MyApp);
