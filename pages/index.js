// Components
import AllTasks from "../components/AllTasks";
import SelectedTask from "../components/SelectedTask";
// Styles
import { Header, Logo } from "../styles/header.styles";
import { StyledHome } from "../styles/index.styles";

// Redux
import { useSelector } from "react-redux";

export default function Home() {
  const displayedTask = useSelector((state) => state.tasks.displayedTask);

  return (
    <div>
      <Header>
        <Logo src="/assets/logo.png" alt="todoistLogo"></Logo>
      </Header>
      <StyledHome>
        <AllTasks />
        {displayedTask && <SelectedTask />}
      </StyledHome>
    </div>
  );
}
